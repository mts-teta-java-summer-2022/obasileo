package com.mts.teta.coursebox.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseUpdateDto {

    @NotBlank(message = "Название курса не может быть пустым")
    private String name;

    @NotBlank(message = "Автор курса не может быть пустым")
    private String author;

    @NotBlank(message = "Описание курса не может быть пустым")
    private String description;

}
