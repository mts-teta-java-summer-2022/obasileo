package com.mts.teta.coursebox.controller;

import com.mts.teta.coursebox.domain.Course;
import com.mts.teta.coursebox.dto.CourseCreateDto;
import com.mts.teta.coursebox.dto.CourseDto;
import com.mts.teta.coursebox.dto.CourseUpdateDto;
import com.mts.teta.coursebox.mapper.CourseMapper;
import com.mts.teta.coursebox.service.CourseService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/courses")
public class CourseController {

    private final CourseService courseService;
    private final CourseMapper courseMapper;

    /**
     * Получить список всех курсов
     * @return - список всех курсов
     */
    @GetMapping
    public ResponseEntity<List<CourseDto>> findAll() {

        return ResponseEntity.ok(courseService
                .findAll()
                .stream()
                .map(courseMapper::courseToCourseDto)
                .collect(Collectors.toList()));
    }

    /**
     * Получить курс по его идентификатору
     * @param id - идентификатор курса
     * @return - возвращает курс
     */
    @GetMapping("/{id}")
    public ResponseEntity<CourseDto> findById(@PathVariable Long id) {
        return ResponseEntity.of(courseService.findById(id).map(courseMapper::courseToCourseDto));
    }

    /**
     * Создать новый курс
     * @param dto - CourseCreateDto
     * @return - возвращает созданный курс
     */
    @PostMapping
    public ResponseEntity<CourseDto> create(@RequestBody @Valid CourseCreateDto dto) {

        Course course = courseService.create(courseMapper.courseCreateDtoToCourse(dto));
        return ResponseEntity.ok(courseMapper.courseToCourseDto(course));
    }

    /**
     * Удалить курс
     * @param id - идентификатор курса
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        courseService.delete(id);
    }

    /**
     * Обновить информацию в курсе
     * @param id - идентификатор курса
     * @param courseUpdateDto - CourseUpdateDto
     * @return - возвращает курс с обновленной информацией
     */
    @PutMapping("/{id}")
    public CourseDto update(@PathVariable Long id,
                            @RequestBody @Valid CourseUpdateDto courseUpdateDto) {
        return courseMapper
                .courseToCourseDto(
                        courseService.update(id, courseMapper.courseUpdateDtoToCourse(courseUpdateDto)));
    }

    /**
     * Получить список курсов по префиксу наименования
     * @param prefix - префикс
     * @return - возвращает список курсов, соответствующих префиксу наименования
     */
    @GetMapping("/filter")
    public ResponseEntity<List<CourseDto>> findByNamePrefix(@RequestBody String prefix) {
        return ResponseEntity.ok(
                courseService.findByNamePrefix(prefix)
                        .stream()
                        .map(courseMapper::courseToCourseDto)
                        .collect(Collectors.toList()));
    }

}
