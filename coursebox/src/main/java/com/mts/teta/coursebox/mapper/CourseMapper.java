package com.mts.teta.coursebox.mapper;

import com.mts.teta.coursebox.domain.Course;
import com.mts.teta.coursebox.dto.CourseCreateDto;
import com.mts.teta.coursebox.dto.CourseDto;
import com.mts.teta.coursebox.dto.CourseUpdateDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseMapper {

    CourseDto courseToCourseDto(Course course);

    Course courseCreateDtoToCourse(CourseCreateDto courseCreateDto);

    Course courseUpdateDtoToCourse(CourseUpdateDto courseUpdateDto);

}
