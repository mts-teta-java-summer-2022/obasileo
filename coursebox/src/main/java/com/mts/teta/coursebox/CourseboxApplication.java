package com.mts.teta.coursebox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CourseboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourseboxApplication.class, args);
	}

}
