package com.mts.teta.coursebox.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class NoSuchElementExceptionDto {

    /**
     * дата события, должна форматироваться в ISO_OFFSET_DATE_TIME
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss[xxx]")
    OffsetDateTime dateOccurred;

    /**
     * сообщение об ошибке
     */
    String message;
}
