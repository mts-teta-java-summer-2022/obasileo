package com.mts.teta.coursebox.repository;

import com.mts.teta.coursebox.domain.Course;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {


    List<Course> findByNameStartingWithIgnoreCase(String prefix);
}
