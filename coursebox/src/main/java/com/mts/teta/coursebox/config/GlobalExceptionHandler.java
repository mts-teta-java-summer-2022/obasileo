package com.mts.teta.coursebox.config;

import com.mts.teta.coursebox.dto.FieldValidationErrorDto;
import com.mts.teta.coursebox.dto.NoSuchElementExceptionDto;
import com.mts.teta.coursebox.dto.RequestValidationErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {

        List<FieldValidationErrorDto> errors =
                ex.getFieldErrors()
                        .stream()
                        .map(error -> new FieldValidationErrorDto(error.getField(), error.getDefaultMessage()))
                        .collect(Collectors.toList());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new RequestValidationErrorDto(errors));

    }

    @ExceptionHandler
    public ResponseEntity<NoSuchElementExceptionDto> noSuchElementExceptionHandler(NoSuchElementException ex) {

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new NoSuchElementExceptionDto(OffsetDateTime.now(), "Курс не найден"));

    }
}
