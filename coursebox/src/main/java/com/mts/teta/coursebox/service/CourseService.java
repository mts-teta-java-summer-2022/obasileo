package com.mts.teta.coursebox.service;

import com.mts.teta.coursebox.domain.Course;
import com.mts.teta.coursebox.repository.CourseRepository;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    @Transactional(readOnly = true)
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Course> findById(Long id) {
        return courseRepository.findById(id);
    }

    public List<Course> findByNamePrefix(String prefix) {
        return courseRepository.findByNameStartingWithIgnoreCase(prefix);
    }

    @Transactional
    public Course create(Course course) {
        return courseRepository.save(course);
    }

    @Transactional
    public void delete(Long id) {

        Course course = courseRepository.findById(id).orElseThrow();
        courseRepository.delete(course);
    }

    @Transactional
    public Course update(Long id, Course updateCourseInfo) {

        Course course = courseRepository.findById(id).orElseThrow();
        return courseRepository.save(course);

    }
}
