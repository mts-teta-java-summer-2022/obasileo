--Task 1
--Количество пользователей, которые не создали ни одного поста.
select count(1)
from profile prof left join post
on prof.profile_id = post.profile_id
where post.post_id is null

--output:
"count"
5

--Task 2
--Выберите ID всех постов по возрастанию, у которых 2 комментария, title начинается с цифры, а длина content больше 20.
--В задании требуется вывести только post_id, но для наглядности добавил title и content
select p.post_id, p.title, p.content
from post p
inner join
(select count(1), post_id
from comment
group by post_id
having count(1) = 2) c
on p.post_id = c.post_id
where  p.title ~ '^\d' and length(p.content) > 20
order by p.post_id

--output:
"post_id","title","content"
22,"22post","aaaaaaaaaaaaaaaaaaaaaa"
24,"24post","aaaaaaaaaaaaaaaaaaaaaaaa"
26,"26post","aaaaaaaaaaaaaaaaaaaaaaaaaa"
28,"28post","aaaaaaaaaaaaaaaaaaaaaaaaaaaa"
32,"32post","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
34,"34post","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
36,"36post","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
38,"38post","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
42,"42post","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
44,"44post","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"

--Task 3
--Выберите первые 3 ID постов по возрастанию, у которых либо нет комментариев, либо он один.
select count(1), p.post_id
from post p
left join comment c on p.post_id = c.post_id
where c.comment_id is null
group by p.post_id
union
select count(1), post_id
from comment
group by post_id
having count(1) = 1
order by post_id
limit 3

--output:
"count","post_id"
1,1
1,3
1,5