package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mts.teta.model.Message;
import com.mts.teta.model.Person;
import com.mts.teta.repository.EnrichedMessagesRepo;
import com.mts.teta.repository.IMessagesRepo;
import com.mts.teta.repository.IPersonRepo;
import com.mts.teta.repository.PersonRepo;
import com.mts.teta.repository.UnEnrichedMessagesRepo;
import com.mts.teta.service.EnrichmentService;
import com.mts.teta.service.MessageValidatorMSISDN;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class EnrichmentServiceTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final IPersonRepo personRepo = new PersonRepo();
    private static final IMessagesRepo enrichedMessagesRepo = new EnrichedMessagesRepo();
    private static final IMessagesRepo unEnrichedMessagesRepo = new UnEnrichedMessagesRepo();

    private static EnrichmentService enrichmentService;

    //Валидный content
    private static String validContent1;
    private static String validContent2;
    private static String validContent3;
    //Невалидный content
    private static String notValidContent1;

    //Ожидаемые результаты
    private static String enrichedContent1;
    private static String enrichedContent2;
    private static String enrichedContent3;

    //Валидное сообщение
    private static Message message1;
    private static Message message2;
    private static Message message3;

    //Невалидное сообщение
    private static Message message4;

    @BeforeAll
    static void beforeAll() throws Exception {

        enrichmentService = new EnrichmentService(new MessageValidatorMSISDN());

        personRepo.add("89161111111", new Person("Дмитрий", "Донской"));
        personRepo.add("89162222222", new Person("Александр", "Суворов"));
        personRepo.add("89163333333", new Person("Михаил", "Кутузов"));

        validContent1 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89161111111\"\n" +
                "}";
        validContent2 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89162222222\"\n" +
                "}";
        validContent3 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89163333333\"\n" +
                "}";

        //Сообщение не соответствует шаблону - нет поля msisdn.
        notValidContent1 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "}";

        //Ожидаемые результаты
        enrichedContent1 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89161111111\",\n" +
                "   \"enrichment\":{\n" +
                "      \"firstName\":\"Дмитрий\",\n" +
                "      \"lastName\":\"Донской\"\n" +
                "   }\n" +
                "}";
        enrichedContent2 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89162222222\",\n" +
                "   \"enrichment\":{\n" +
                "      \"firstName\":\"Александр\",\n" +
                "      \"lastName\":\"Суворов\"\n" +
                "   }\n" +
                "}";
        enrichedContent3 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89163333333\",\n" +
                "   \"enrichment\":{\n" +
                "      \"firstName\":\"Михаил\",\n" +
                "      \"lastName\":\"Кутузов\"\n" +
                "   }\n" +
                "}";

        //Валидные сообщения
        message1 = new Message(validContent1, Message.EnrichmentType.MSISDN);
        message2 = new Message(validContent2, Message.EnrichmentType.MSISDN);
        message3 = new Message(validContent3, Message.EnrichmentType.MSISDN);
        //Невалидное сообщение
        message4 = new Message(notValidContent1, Message.EnrichmentType.MSISDN);

    }

    @BeforeEach
    void beforeEach() {
        // Перед каждым тестом «очищаются» репозитории EnrichedMessagesRepo и UnEnrichedMessagesRepo
        enrichedMessagesRepo.clear();
        unEnrichedMessagesRepo.clear();
    }

    /**
     * Тест на добавление новой ноды в Json
     */
    @Test
    void shouldAddJsonNode () throws JSONException, JsonProcessingException {

        JSONAssert.assertEquals(
                enrichedContent1,
                enrichmentService
                        .addOrUpdateNode(
                                validContent1,
                                "enrichment",
                                new Person("Дмитрий", "Донской")),
                JSONCompareMode.STRICT);
    }

    /**
     * Тест на обновление существующей ноды в Json
     */
    @Test
    void shouldUpdateJsonNode () throws JSONException, JsonProcessingException {

        JSONAssert.assertEquals(
                enrichedContent1,
                enrichmentService
                        .addOrUpdateNode(
                                validContent1,
                                "enrichment",
                                new Person("Дмитрий", "Донской")),
                JSONCompareMode.STRICT);
    }

    /**
     * Тест на получпение msisdn
     * @throws JsonProcessingException
     */
    @Test
    void shouldReturnMsisdnValueFromContent() throws JsonProcessingException {

        assertEquals("89161111111", enrichmentService.getMisdn(validContent1));

    }

    /**
     * Тест метода enrich(...) в одном потоке
     * @throws JSONException
     */
    @Test
    void enrichSingleThreadTest() throws JSONException {

        Person person = personRepo.get("89161111111");
        JSONAssert.assertEquals(enrichedContent1, enrichmentService.enrich(message1), JSONCompareMode.STRICT);

    }

    /**
     * Тест метода enrich(...) в многопоточной среде
     * @throws JSONException
     */
    @Test
    void enrichMultithreadingTest() throws ExecutionException, InterruptedException, JSONException {

        CompletableFuture<String> future1 = future1 = CompletableFuture
                    .supplyAsync(() -> enrichmentService.enrich(message1));

        CompletableFuture<String> future2 = future2 = CompletableFuture
                    .supplyAsync(() -> enrichmentService.enrich(message2));

        CompletableFuture<String> future3 = future3 = CompletableFuture
                    .supplyAsync(() -> enrichmentService.enrich(message3));

        CompletableFuture<String> future4 = future4 = CompletableFuture
                .supplyAsync(() -> enrichmentService.enrich(message4));

        JSONAssert.assertEquals(enrichedContent1, future1.get(), JSONCompareMode.STRICT);
        JSONAssert.assertEquals(enrichedContent2, future2.get(), JSONCompareMode.STRICT);
        JSONAssert.assertEquals(enrichedContent3, future3.get(), JSONCompareMode.STRICT);

        //Невалидное сообщение остается без изменений
        JSONAssert.assertEquals(notValidContent1, future4.get(), JSONCompareMode.STRICT);

        //В репозиторий обогащенных сообщений должно быть добавлено 3 сообщения,
        // в репозиторий необогащенных 1.
        assertEquals(3, enrichedMessagesRepo.size());
        assertEquals(1, unEnrichedMessagesRepo.size());
        }
}
