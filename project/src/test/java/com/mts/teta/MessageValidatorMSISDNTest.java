package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.mts.teta.model.Message;
import com.mts.teta.service.MessageValidatorMSISDN;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class MessageValidatorMSISDNTest {

    private static final MessageValidatorMSISDN validator = new MessageValidatorMSISDN();

    private static String validContent1;
    private static String notValidContent1;
    private static String notValidContent2;

    @BeforeAll
    static void beforeAll() throws Exception {

        validContent1 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"89161111111\"\n" +
                "}";

        //msisdn не соответствует шаблону - должно быть 11 цифр. В данном примере 10.
        notValidContent1 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "   \"msisdn\":\"8916333333\"\n" +
                "}";
        //Сообщение не соответствует шаблону - нет поля msisdn.
        notValidContent2 = "{\n" +
                "   \"action\":\"button_click\",\n" +
                "   \"page\":\"book_card\",\n" +
                "}";
    }

    @Test
    void shouldReturnTrueIfContentValidOrFalseIfNot() {

        Message validMessage1 = new Message(validContent1, Message.EnrichmentType.MSISDN);
        Message notValidMessage1 = new Message(notValidContent1, Message.EnrichmentType.MSISDN);
        Message notValidMessage2 = new Message(notValidContent2, Message.EnrichmentType.MSISDN);

        assertTrue(validator.isValid(validMessage1));
        assertFalse(validator.isValid(notValidMessage1));
        assertFalse(validator.isValid(notValidMessage2));

    }

}
