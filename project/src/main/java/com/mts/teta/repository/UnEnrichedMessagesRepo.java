package com.mts.teta.repository;

import com.mts.teta.model.Message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Репозиторий необогащенных сообщений
 */
public class UnEnrichedMessagesRepo implements IMessagesRepo {

    private static final List<Message> unEnrichedMessages = Collections.synchronizedList(new ArrayList<>());

    public boolean add(Message message) {
        return unEnrichedMessages.add(message);
    }

    public int size() {
        return unEnrichedMessages.size();
    }

    public void clear() {
        unEnrichedMessages.clear();
    }
}
