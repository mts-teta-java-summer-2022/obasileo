package com.mts.teta.repository;

import com.mts.teta.model.Message;

public interface IMessagesRepo {

    boolean add(Message message);

    int size();

    void clear();


}
