package com.mts.teta.repository;

import com.mts.teta.model.Person;

public interface IPersonRepo {

    void add(String msisdn, Person person);

    public Person get(String msisdn);

}
