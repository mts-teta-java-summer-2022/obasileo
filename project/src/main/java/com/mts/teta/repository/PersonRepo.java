package com.mts.teta.repository;

import com.mts.teta.model.Person;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Репозиторий данных о человеке
 */
public class PersonRepo implements IPersonRepo {

    private static final Map<String, Person> persons = new ConcurrentHashMap<>();

    public void add(String msisdn, Person person) {
       persons.put(msisdn, person);
    }

    public Person get(String msisdn) {
        return persons.get(msisdn);
    }

}
