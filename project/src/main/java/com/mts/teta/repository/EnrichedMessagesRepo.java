package com.mts.teta.repository;

import com.mts.teta.model.Message;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Репозиторий обогащенных сообщений
 */
public class EnrichedMessagesRepo implements IMessagesRepo {

    private static final List<Message> enrichedMessages = Collections.synchronizedList(new ArrayList<>());

    public boolean add(Message message) {
        return enrichedMessages.add(message);
    }

    public int size() {
        return enrichedMessages.size();
    }

    public void clear() {
        enrichedMessages.clear();
    }
}
