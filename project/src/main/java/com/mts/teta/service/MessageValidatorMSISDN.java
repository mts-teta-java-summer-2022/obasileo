package com.mts.teta.service;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mts.teta.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageValidatorMSISDN implements MessageValidator {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String MSISDN_PATTERN = "\\d{11}";

    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * Проверяет на валидность входящее сообщение
     * @param message - входящее сообщение
     * @return - true, если сообщение валидное
     *         - false, если сообщение невалидное
     */
    @Override
    public boolean isValid(Message message) {

        JsonNode rootNode = null;

        String content = message.getContent();
        Message.EnrichmentType enrichmentType = message.getEnrichmentType();

        if (content != null && enrichmentType != null && enrichmentType == Message.EnrichmentType.MSISDN) {
            try {
                rootNode = mapper.readTree(content);
            } catch (JacksonException e) {
                logger.error("Invalid json string {} ", message.getContent());
                return false;
            }
            JsonNode msisdnNode = rootNode.get("msisdn");
            if (msisdnNode != null && msisdnNode.textValue().matches(MSISDN_PATTERN)) {
                return true;
            }
        }

        return false;
    }

}
