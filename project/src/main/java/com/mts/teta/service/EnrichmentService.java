package com.mts.teta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mts.teta.model.Message;
import com.mts.teta.model.Person;
import com.mts.teta.repository.EnrichedMessagesRepo;
import com.mts.teta.repository.IMessagesRepo;
import com.mts.teta.repository.IPersonRepo;
import com.mts.teta.repository.PersonRepo;
import com.mts.teta.repository.UnEnrichedMessagesRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnrichmentService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ObjectMapper mapper = new ObjectMapper();

    private final IMessagesRepo enrichedMessagesRepo = new EnrichedMessagesRepo();

    private final IMessagesRepo unEnrichedMessagesRepo = new UnEnrichedMessagesRepo();

    private final IPersonRepo personRepo = new PersonRepo();

    private final MessageValidator validator;

    public EnrichmentService(MessageValidator validator) {
        this.validator = validator;
    }

    /**
     * Обогащает сообщение
     * @param message - сообщение
     * @return - возвращает обогащенную строку в формате JSON
     */
    public synchronized String enrich(Message message) {

        //Убедиться, что выполняется в разных потоках
        logger.info(Thread.currentThread().getName());

        if (!validator.isValid(message)) {
            logger.info("message not valid");
            unEnrichedMessagesRepo.add(message);
            return message.getContent();
        }

        String msisdn = null;
        try {
            msisdn = getMisdn(message.getContent());
        } catch (JsonProcessingException e) {
            logger.error("get msisdn error: {}", e.getMessage());
            unEnrichedMessagesRepo.add(message);
            return message.getContent();
        }

        Person person = personRepo.get(msisdn);
        if (person == null) {
            logger.info("person nof found by msisdn: {}", msisdn);
            unEnrichedMessagesRepo.add(message);
            return message.getContent();
        }

        String enrichedContent = null;
        try {
            enrichedContent = addOrUpdateNode(message.getContent(), "enrichment", person);
        } catch (JsonProcessingException e) {
            logger.error("enrich error: {}", e.getMessage());
            unEnrichedMessagesRepo.add(message);
            return message.getContent();
        }

        enrichedMessagesRepo.add(message);
        return enrichedContent;
    }

    /**
     * Добавить новую ноду или обновить существующую
     * @param content - строка в формате JSON
     * @param fieldName - нода в JSON
     * @param pojo - добавляемый JSON объект
     * @return - возвращает обновленный JSON
     * @throws JsonProcessingException
     */
    public String addOrUpdateNode(String content, String fieldName, Object pojo) throws JsonProcessingException {

        JsonNode rootNode = mapper.readTree(content);
        ((ObjectNode) rootNode).putPOJO(fieldName, pojo);

        return rootNode.toString();
    }

    /**
     * Получить значение msisdn
     * @param content - строка в формате JSON
     * @return - возвращает значение msisdn
     * @throws JsonProcessingException
     */
    public String getMisdn(String content) throws JsonProcessingException {

        JsonNode rootNode = mapper.readTree(content);
        JsonNode msisdnNode = rootNode.get("msisdn");
        return msisdnNode.textValue();
    }
}
