package com.mts.teta.service;

import com.mts.teta.model.Message;

public interface MessageValidator {

    boolean isValid(Message message);

}
